import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Navbar from './Navbar';

import logo from '../assets/img/logo@2x.png';

class Header extends Component {
  constructor() {
    super();

    this.state = {
      navActive: false
    };
  }

  handleClick = (event) => {
    event.preventDefault();

    const { navActive } = this.state;

    this.setState({
      navActive: !navActive
    });
  }

  render() {
    const { navActive } = this.state;
    const toggleClass = (navActive ? 'nav__toggle active' : 'nav__toggle');

    return (
      <header className='header'>
        <div className='header__container container'>
          <button
            onClick={this.handleClick}
            className={toggleClass}
            type='button'
            data-toggle='collapse'>
            <span className='nav__toggle__icon' />
          </button>

          <strong className='logo'>
            <Link to='/'>
              <img src={logo} alt='News' />
            </Link>
          </strong>

          <Navbar active={navActive} />
        </div>
      </header>
    );
  }
}

export default Header;
