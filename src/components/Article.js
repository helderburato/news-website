import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { slug } from '../utils/string';

class Article extends PureComponent {
  static defaultProps = {
    description: '',
    picture: '',
    size: 'small'
  };

  static propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
    picture: PropTypes.string,
    category: PropTypes.string.isRequired,
    author: PropTypes.instanceOf(Object).isRequired,
    size: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      categorySlug: '',
      categoryParsed: ''
    };
  }

  componentWillMount() {
    this.updateCategoryName();
  }

  setCategorySlug = (categoryParsed) => {
    const categorySlug = slug(categoryParsed);

    this.setState({ categorySlug });
  }

  updateCategoryName() {
    const { category } = this.props;
    const categoryNumber = category.split('-')[1];
    let categoryParsed;

    switch (categoryNumber) {
      case '1':
        categoryParsed = 'Politics';
        break;

      case '2':
        categoryParsed = 'Business';
        break;

      case '3':
        categoryParsed = 'Tech';
        break;

      case '4':
        categoryParsed = 'Science';
        break;

      case '5':
        categoryParsed = 'Sports';
        break;

      default:
        categoryParsed = 'General';
        break;
    }

    this.setCategorySlug(categoryParsed);

    this.setState({ categoryParsed });
  }

  renderPicture() {
    const {
      title,
      picture,
      id
    } = this.props;
    const { categorySlug } = this.state;

    if (!picture) return null;

    return (
      <figure className='article__figure'>
        <img className='article__image' src={picture} alt={title} />

        <div className='article__readmore'>
          <Link to={`/news/${categorySlug}/${id}`} className='button button__shadow'>
            Read More
          </Link>
        </div>
      </figure>
    );
  }

  renderAuthor() {
    const { author } = this.props;
    const { photo, name } = author;

    return (
      <div className='article__author'>
        <span className='article__author__photo'>
          <img src={photo} alt={name} />
        </span>
        <i className='article__author__name'>by {name}</i>
      </div>
    );
  }

  renderDescription() {
    const {
      description,
      size,
      id
    } = this.props;
    const { categorySlug } = this.state;

    if (!description || size === 'large') return null;

    return (
      <p className='article__description'>
        <Link to={`/news/${categorySlug}/${id}`}>{description}</Link>
      </p>
    );
  }

  render() {
    const {
      id,
      title,
      size
    } = this.props;

    const {
      categorySlug,
      categoryParsed
    } = this.state;

    return (
      <article className={`article ${size}`}>
        <div className='article__divider'>
          <Link to={`/news/${categorySlug}`} className={`article__tag article__tag__${categorySlug}`}>
            {categoryParsed}
          </Link>
        </div>

        {this.renderPicture()}

        <h1 className='article__title'>
          <Link to={`/news/${categorySlug}/${id}`}>{title}</Link>
        </h1>

        {this.renderAuthor()}

        {this.renderDescription()}
      </article>
    );
  }
}

export default Article;
