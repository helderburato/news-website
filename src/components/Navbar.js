import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class Navbar extends PureComponent {
  static defaultProps = {
    active: false
  };

  static propTypes = {
    active: PropTypes.bool
  };

  render() {
    const { active } = this.props;
    const navClass = (active ? 'nav active' : 'nav');

    return (
      <nav className={navClass} role='navigation' aria-label='Main navigation'>
        <ul>
          <li className='nav__item politics'>
            <Link to='/news/politics'>Politics</Link>
          </li>
          <li className='nav__item business'>
            <Link to='/news/business'>Business</Link>
          </li>
          <li className='nav__item tech'>
            <Link to='/news/tech'>Tech</Link>
          </li>
          <li className='nav__item science'>
            <Link to='/news/science'>Science</Link>
          </li>
          <li className='nav__item sports'>
            <Link to='/news/sports'>Sports</Link>
          </li>
          <li className='nav__item nav__item__login'>
            <Link className='login__area' to='/login'>Login</Link>
          </li>
        </ul>
      </nav>
    );
  }
}

export default Navbar;
