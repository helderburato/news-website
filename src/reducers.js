import { combineReducers } from 'redux';
import articlesReducer from './articles/reducers';
import authReducer from './auth/reducers';

// Combine all app's reducers
const rootReducer = combineReducers({
  articles: articlesReducer,
  auth: authReducer
});

export default rootReducer;
