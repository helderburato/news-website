import types from './types';

export const changeField = (field, value) => (dispatch) => {
  dispatch({
    type: types.AUTH_UPDATE_FIELD,
    payload: {
      field,
      value
    }
  });
};
