import types from './types';

const INITIAL_STATE = {
  username: '',
  password: ''
};

const authReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case (types.AUTH_UPDATE_FIELD): {
      const { payload } = action;
      const { field, value } = payload;
      return { ...state, ...{ [field]: value } };
    }

    default: {
      return state;
    }
  }
};

export default authReducer;
