import { connect } from 'react-redux';
import AuthPage from './AuthPage';

import { changeField } from './actions';

const mapStateToProps = (state) => {
  const { auth } = state;
  return { auth };
};

const mapDispatchToProps = dispatch => ({
  changeField: (field, value) => dispatch(changeField(field, value))
});

const AuthContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthPage);

export default AuthContainer;
