import React, { Component } from 'react';
import { Helmet } from 'react-helmet';

import { isNotEmpty } from '../utils/string';

class AuthPage extends Component {
  constructor() {
    super();

    this.state = {
      formValid: ''
    };
  }

  handleSubmit = (event) => {
    event.preventDefault();

    const formValid = this.validate();

    if (formValid) {
      const { history } = this.props;
      history.push('/profile');
    } else {
      this.setState({ formValid });
    }
  }

  validate() {
    const {
      auth: {
        username,
        password
      }
    } = this.props;
    let valid = false;

    if (isNotEmpty(username) && isNotEmpty(password)) {
      valid = true;
    }
    return valid;
  }

  renderValidation() {
    const { formValid } = this.state;

    if (formValid === '') return null;

    const alertClass = (!formValid ? 'alert__danger' : 'alert__success');
    const alertMessage = (!formValid ? 'Make sure the fields are filled in correctly' : 'Login done successfully!');

    return (
      <p className={`alert ${alertClass}`}>{alertMessage}</p>
    );
  }

  render() {
    const { changeField } = this.props;

    return (
      <article id='auth' className='auth content'>
        <Helmet>
          <title>User Area</title>
        </Helmet>

        <section className='container'>
          <h1 className='auth__title'>User area</h1>

          {this.renderValidation()}

          <form className='form form__login'>
            <div className='form__row'>
              <label className='form__label' htmlFor='username'>Username</label>
              <input
                onChange={(event) => {
                  const { value } = event.target;
                  changeField('username', value);
                }}
                className='input'
                id='username'
                type='text'
                name='username' />
            </div>
            <div className='form__row'>
              <label className='form__label' htmlFor='password'>Password</label>
              <input
                onChange={(event) => {
                  const { value } = event.target;
                  changeField('password', value);
                }}
                className='input'
                id='password'
                type='password'
                name='password' />
            </div>
            <div className='submit'>
              <button
                onClick={this.handleSubmit}
                type='submit'
                className='form__button button button__default'>
                Login
              </button>
            </div>
          </form>
        </section>
      </article>
    );
  }
}

export default AuthPage;
