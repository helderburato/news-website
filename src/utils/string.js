const slug = string => string.toLowerCase()
  .replace(/[^\w\s-]/g, '')
  .replace(/[\s_-]+/g, '-')
  .replace(/^-+|-+$/g, '');

const isNotEmpty = value => (value !== null && value !== '' && typeof value !== 'undefined');

export {
  slug,
  isNotEmpty
};
