import { connect } from 'react-redux';
import HomePage from './HomePage';

import { fetchTrends } from '../articles/actions';

const mapStateToProps = (state) => {
  const { articles } = state;
  const { results } = articles;
  return { results };
};

const mapDispatchToProps = dispatch => ({
  fetchTrends: () => dispatch(fetchTrends())
});

const HomeContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);

export default HomeContainer;
