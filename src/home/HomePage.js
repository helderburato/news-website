import React, { Component } from 'react';
import shortid from 'shortid';

import Article from '../components/Article';

class HomePage extends Component {
  componentDidMount() {
    const { fetchTrends } = this.props;
    fetchTrends();
  }

  renderArticles = () => {
    const { results } = this.props;
    if (!results) return null;
    let indexArticles = 1;

    const articlesNodes = results.map((article, index) => {
      const nodes = [];

      nodes.push(this.renderArticle(article, index));
      if (this.hasBreakLine(indexArticles, results.length)) nodes.push(<hr key={shortid.generate()} className='article__breakline' />);

      indexArticles += 1;
      return nodes;
    });
    return articlesNodes;
  }

  hasBreakLine = (index, size) => {
    const BREAK_LINE = 3;
    return (index % BREAK_LINE === 0 && index !== size);
  }

  renderArticle = (article, index) => {
    if (!article) return null;

    const {
      id,
      author,
      authorAvatar,
      category,
      title,
      description,
      url,
      imageUrl
    } = article;

    const colummClass = this.getColumnClass(index);
    const size = (index === 0 ? 'large' : 'small');

    return (
      <li key={shortid.generate()} className={`article__wrapper ${colummClass}`}>
        <Article
          key={shortid.generate()}
          id={id}
          title={title}
          url={url}
          category={category}
          picture={imageUrl}
          author={{
            name: author,
            photo: authorAvatar
          }}
          size={size}
          description={description} />
      </li>
    );
  }

  getColumnClass = (index) => {
    let columnClass = 'col-md-6 col-sm-12';

    if (index === 1 || index === 2) columnClass = 'col-md-3 col-sm-6';
    if (index > 2) columnClass = 'col-md-4 col-sm-6';

    return columnClass;
  }

  render() {
    return (
      <article id='home' className='content'>
        <section className='container'>
          <ul className='row'>
            {this.renderArticles()}
          </ul>
        </section>
      </article>
    );
  }
}

export default HomePage;
