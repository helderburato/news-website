const API_URL = 'https://5bc91b8657adaa001375b13a.mockapi.io/api/';

class NewsAPI {
  static getLatest() {
    return NewsAPI.createRequest('articles');
  }

  static createRequest = (url, params = {}) => {
    const urlWithParams = NewsAPI.createUrlWithParams(url, params);

    return fetch(urlWithParams, {
      method: 'GET',
      mode: 'cors'
    }).then(response => response.json());
  }

  static createUrlWithParams(url = '', params) {
    const parsedParams = NewsAPI.parseUrlParams(params);
    const parsedUrl = (url ? `${API_URL}${url}?${parsedParams}` : `${API_URL}${parsedParams}`);

    console.info(`✔️ Requested ${parsedUrl}`);

    return parsedUrl;
  }

  static parseUrlParams = (params) => {
    const esc = encodeURIComponent;
    const query = Object.keys(params)
      .map(k => `${esc(k)}=${esc(params[k])}`)
      .join('&');
    return query;
  }
}

export default NewsAPI;
