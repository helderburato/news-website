import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { applyMiddleware, compose, createStore } from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';

import history from './history';
import reducers from '../reducers';

let store;
let persistedState;

const configureStore = () => {
  store = createStore(
    connectRouter(history)(reducers),
    compose(
      applyMiddleware(
        routerMiddleware(history),
        thunk,
        logger
      )
    )
  );
  return store;
};

const getStore = () => {
  if (store) return store;
  store = configureStore(persistedState);
  return store;
};

export default getStore;
