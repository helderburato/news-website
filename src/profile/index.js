import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';

class ProfilePage extends Component {
  render() {
    const {
      auth: { username }
    } = this.props;

    return (
      <article id='profile' className='auth profile content'>
        <Helmet>
          <title>Profile</title>
        </Helmet>

        <section className='container'>
          <h1 className='auth__title'>Welcome, <span>{username}</span></h1>

          <div className='profile__container'>
            <h3>My interests</h3>

            <form className='form form__profile'>
              <ul className='interests'>
                <li className='interests__item'>
                  <span className='button button__rounded button__rounded__politics'>Politics</span>
                </li>
                <li className='interests__item'>
                  <span className='button button__rounded button__rounded__business'>Business</span>
                </li>
                <li className='interests__item'>
                  <span className='button button__rounded button__rounded__tech'>Tech</span>
                </li>
                <li className='interests__item'>
                  <span className='button button__rounded button__rounded__science'>Science</span>
                </li>
                <li className='interests__item'>
                  <span className='button button__rounded button__rounded__sports'>Sports</span>
                </li>
              </ul>

              <div className='form__profile__submit'>
                <button type='submit' className='form__button button button__default'>
                  Save
                </button>
              </div>
            </form>

            <div className='profile__back'>
              <Link className='profile__back__link' to='/'>
                Back to home
              </Link>
            </div>
          </div>
        </section>
      </article>
    );
  }
}

const mapStateToProps = (state) => {
  const { auth } = state;
  return { auth };
};

export default connect(mapStateToProps, null)(ProfilePage);
