import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { Route, Switch } from 'react-router';

import Header from '../components/Header';
import HomePage from '../home';
import AuthPage from '../auth';
import ProfilePage from '../profile';
import NotFoundPage from '../errors';

class App extends Component {
  render() {
    return (
      <div>
        <Helmet
          titleTemplate='%s - News'
          defaultTitle='News'>
          <meta
            name='description'
            content='A development project for a news website.' />
        </Helmet>
        <Header />
        <Switch>
          <Route exact path='/' component={HomePage} />
          <Route path='/login' component={AuthPage} />
          <Route path='/profile' component={ProfilePage} />
          <Route component={NotFoundPage} />
        </Switch>
      </div>
    );
  }
}

export default App;
