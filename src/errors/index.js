import React from 'react';
import { Helmet } from 'react-helmet';

const NotFoundPage = () => (
  <article id='not-found' className='content'>
    <Helmet>
      <title>Página não encontrada</title>
    </Helmet>

    <section className='container'>
      <h1>404</h1>

      <p>Página não encontrada :(</p>
    </section>
  </article>
);

export default NotFoundPage;
