import types from './types';

const INITIAL_STATE = {
  results: [],
  loading: false
};

const articlesReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.ARTICLES_LATEST: {
      return { ...state, ...{ results: action.payload, loading: false } };
    }

    case types.ARTICLES_LOADING: {
      return { ...state, loading: action.payload };
    }

    default: {
      return state;
    }
  }
};

export default articlesReducer;
