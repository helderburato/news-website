import types from './types';

import NewsAPI from '../services/NewsAPI';

export const setLoading = (loading = false) => (dispatch) => {
  dispatch({
    type: types.ARTICLES_LOADING,
    payload: loading
  });
};

export function fetchTrends() {
  return async (dispatch) => {
    setLoading(true);

    try {
      const articles = await NewsAPI.getLatest();

      dispatch({
        type: types.ARTICLES_LATEST,
        payload: articles
      });
    } catch (e) {
      console.log(e);
    }
  };
}
