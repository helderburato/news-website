import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { connectRouter, ConnectedRouter } from 'connected-react-router';

import Store from './config/reduxStore';
import history from './config/history';
import rootReducer from './reducers';
import App from './app';

import './stylesheets/main.scss';

const render = (Component) => {
  ReactDOM.render(
    <Provider store={Store()}>
      <ConnectedRouter history={history}>
        <Component />
      </ConnectedRouter>
    </Provider>,
    document.querySelector('[data-js="app"]'),
  );
};

render(App);

// Hot reloading
if (module.hot) {
  // Reload components
  module.hot.accept('./app', () => {
    const NextApp = require('./app').default;
    render(NextApp);
  });

  // Reload reducers
  module.hot.accept('./reducers', () => {
    Store().replaceReducer(connectRouter(history)(rootReducer));
  });
}
