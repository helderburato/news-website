# 📰 News Website

> A development project for a news website.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* [NodeJS](https://nodejs.org/en/);
* [Yarn](https://yarnpkg.com/lang/en/).

### Installing

A step by step series of examples that tell you how to get a development env running

Clone the project in your local machine:

```bash
$ git clone git@bitbucket.org:hrberto/news-website.git
```

Install packages using `Yarn`:
```bash
$ yarn install ./news-website
```

Start project locally:
```bash
$ yarn start
```

## Running the tests

Execute all project tests running:
```bash
$ yarn test
```

### And coding style tests

The tests verify the quality of code and the written tests.

This project uses [Airbnb React/JSX Style Guide](https://github.com/airbnb/javascript/tree/master/react).

## Deployment

To create a build of project, execute the:
```bash
$ yarn run build
```

## Built With

* [React](https://reactjs.org/) - A JavaScript library for building user interfaces.
* [Redux](https://redux.js.org/) - Redux is a predictable state container for JavaScript apps.
* [SASS](https://sass-lang.com/) - CSS with superpowers.
  * Sass architecture folders based in [sass Guidelines](https://sass-guidelin.es/#architecture).

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## Author

Helder Burato Berto - [hrberto](https://bitbucket.org/hrberto)
